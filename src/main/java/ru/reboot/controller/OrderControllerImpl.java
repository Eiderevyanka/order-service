package ru.reboot.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.reboot.dto.Order;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "orders")
public class OrderControllerImpl implements OrderController {

    private static final Logger logger = LogManager.getLogger(OrderControllerImpl.class);

    private OrderService orderService;

    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("info")
    public String info() {
        logger.info("method .info invoked");
        return "OrderController " + new Date();
    }

    @Override
    @GetMapping("/all")
    public List<Order> getAllOrders() {
        return orderService.getAllOrders();
    }

    @Override
    @GetMapping("/order")
    public Order getOrderById(@RequestParam("orderId") String orderId) {
        return orderService.getOrderById(orderId);
    }

    @Override
    @GetMapping("/all/byPhoneNumber")
    public List<Order> getOrdersByPhoneNumber(@RequestParam("phoneNumber") String phoneNumber) {
        return orderService.getOrdersByPhoneNumber(phoneNumber);
    }

    @Override
    @PostMapping("/order")
    public Order createOrder(@RequestBody Order order) throws Exception {
        return orderService.createOrder(order);
    }

    @Override
    @DeleteMapping("/order")
    public void deleteOrder(@RequestParam("orderId") String orderId) throws Exception {
        orderService.deleteOrder(orderId);
    }

    @ExceptionHandler(BusinessLogicException.class)
    public ResponseEntity<Map<String, String>> handleException(BusinessLogicException ex, HttpServletRequest request) {

        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        Map<String, String> map = new HashMap<>();

        map.put("errorCode", ex.getCode());
        map.put("message", ex.getMessage());
        map.put("timestamp", LocalDateTime.now().toString());
        map.put("status", String.valueOf(status.value()));
        map.put("error", status.getReasonPhrase());
        map.put("path", request.getRequestURI());

        return new ResponseEntity<>(map, status);
    }
}
