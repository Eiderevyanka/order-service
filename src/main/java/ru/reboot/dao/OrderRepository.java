package ru.reboot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.reboot.dao.entity.OrderEntity;

import javax.transaction.Transactional;
import java.util.List;

public interface OrderRepository extends JpaRepository<OrderEntity, String> {

    @Transactional
    void deleteById(int id);

    List<OrderEntity> findAllByOrderId(String orderId);

    @Query("select distinct orderId from OrderEntity")
    List<String> findDistinctOrderId();
}

