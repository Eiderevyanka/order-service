package ru.reboot.dao.entity;


import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name = "orders")
public class OrderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "order_id")
    private String orderId;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "address")
    private String address;

    @Column(name = "order_date")
    private String orderDate;

    @Column(name = "delivery_date")
    private String deliveryDate;

    @Column(name = "item_id")
    private String itemId;

    @Column(name = "count")
    private int count;

    public OrderEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "OrderEntity{" +
                "id='" + id + '\'' +
                ", orderId='" + orderId + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                ", orderDate=" + orderDate +
                ", deliveryDate=" + deliveryDate +
                ", itemId='" + itemId + '\'' +
                ", count=" + count +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderEntity that = (OrderEntity) o;
        return id == that.id && count == that.count && orderId.equals(that.orderId) && phoneNumber.equals(that.phoneNumber) && address.equals(that.address) && orderDate.equals(that.orderDate) && deliveryDate.equals(that.deliveryDate) && itemId.equals(that.itemId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, orderId, phoneNumber, address, orderDate, deliveryDate, itemId, count);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private final OrderEntity orderEntity;

        private Builder() {
            orderEntity = new OrderEntity();
        }

        public Builder orderId(String orderId) {
            orderEntity.orderId = orderId;
            return this;
        }

        public Builder phoneNumber(String phoneNumber) {
            orderEntity.phoneNumber = phoneNumber;
            return this;
        }

        public Builder address(String address) {
            orderEntity.address = address;
            return this;
        }

        public Builder orderDate(String orderDate) {
            orderEntity.orderDate = orderDate;
            return this;
        }

        public Builder deliveryDate(String deliveryDate) {
            orderEntity.deliveryDate = deliveryDate;
            return this;
        }

        public Builder itemId(String itemId) {
            orderEntity.itemId = itemId;
            return this;
        }

        public Builder count(int count) {
            orderEntity.count = count;
            return this;
        }

        public OrderEntity build() {
            return orderEntity;
        }
    }
}
