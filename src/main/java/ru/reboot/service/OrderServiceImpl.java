package ru.reboot.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import ru.reboot.dao.OrderRepository;
import ru.reboot.dao.entity.OrderEntity;
import ru.reboot.dto.Order;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCodes;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    private static final Logger logger = LogManager.getLogger(OrderServiceImpl.class);

    @Value("${client.stock-service}")
    private String stockServiceUrl;

    private OrderRepository orderRepository;
    private ObjectMapper mapper;
    private RestTemplate restTemplate;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Autowired
    public void setOrderRepository(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Autowired
    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public List<Order> getAllOrders() {
        logger.info("Method .getAllOrders started");

        try {

            List<Order> orders = new ArrayList<>();

            List<String> distinctOrderId = this.orderRepository.findDistinctOrderId();

            if (!distinctOrderId.isEmpty()) {
                for (String s : distinctOrderId) {
                    List<OrderEntity> orderEntities = this.orderRepository.findAllByOrderId(s);
                    Order order = this.convertOrderEntityToOrder(orderEntities);
                    orders.add(order);
                }
            }
            logger.info("Method .getAllOrders completed allOrders={}", orders);
            return orders;

        } catch (Exception ex) {
            logger.error("Failed to .getAllOrders error={}", ex.toString(), ex);
            throw ex;
        }
    }

    @Override
    public Order getOrderById(String orderId) {
        logger.info("Method .getOrderById started orderId={}", orderId);

        try {
            if (Objects.isNull(orderId) || orderId.equals("")) {
                throw new BusinessLogicException("Order Id can't be null or empty ", ErrorCodes.ILLEGAL_ARGUMENT.toString());
            }

            List<OrderEntity> entityList = this.orderRepository.findAllByOrderId(orderId);

            if (entityList.isEmpty()) {
                throw new BusinessLogicException("Order not found ", ErrorCodes.ORDER_NOT_FOUND.toString());
            }

            Order order = convertOrderEntityToOrder(entityList);
            logger.info("Method .getOrderById completed order={}", order);
            return order;
        } catch (Exception e) {
            logger.error("Failed to .getOrderById error={}", e.toString(), e);
            throw e;
        }
    }

    @Override
    public List<Order> getOrdersByPhoneNumber(String phoneNumber) {
        logger.info("Method .getOrdersByPhoneNumber started phoneNumber={}", phoneNumber);

        try {
            if (Objects.isNull(phoneNumber) || phoneNumber.equals("")) {
                throw new BusinessLogicException("Phone Number can't be null or empty ", ErrorCodes.ILLEGAL_ARGUMENT.toString());
            }

            List<Order> orderList = getAllOrders().stream()
                    .filter(order -> order.getPhoneNumber().equals(phoneNumber)).collect(Collectors.toList());

            logger.info("Method .getOrdersByPhoneNumber completed orders={}", orderList);
            return orderList;

        } catch (Exception ex) {
            logger.error("Failed to .getOrdersByPhoneNumber error={}", ex.toString(), ex);
            throw ex;
        }
    }

    @Override
    public Order createOrder(Order order) throws Exception {

        try {
            logger.info("Method .createOrder");

            validateOrder(order);

            if (!orderRepository.findAllByOrderId(order.getOrderId()).isEmpty()) {
                throw new BusinessLogicException("Order already exists", ErrorCodes.DUPLICATE_ORDER.toString());
            }

            String url = stockServiceUrl + "/stock/reserve?";
            HttpEntity<String> request = new HttpEntity<>(new HttpHeaders());

            try {
                convertOrderToOrderEntities(order).forEach(entity -> {
                    String params = "itemId=" + entity.getItemId() + "&count=" + entity.getCount();
                    restTemplate.postForEntity(url + params, request, String.class);
                    orderRepository.save(entity);
                });
            } catch (HttpServerErrorException.InternalServerError e) {
                Map<String, String> map = mapper.readValue(e.getResponseBodyAsString(), Map.class);
                if (map.containsKey("errorCode")) {
                    throw new BusinessLogicException(map.get("message"), map.get("errorCode"));
                } else {
                    throw e;
                }
            }

            logger.info("Method .createOrder completed order={}", order);
            return order;

        } catch (Exception e) {
            logger.error("Failed to .createOrder error={}", e.toString(), e);
            throw e;
        }
    }

    @Override
    public void deleteOrder(String orderId) throws Exception {

        try {
            logger.info("Method .deleteOrder");

            if (Objects.isNull(orderId) || orderId.equals("")) {
                throw new BusinessLogicException("Order id can't be null or empty", ErrorCodes.ILLEGAL_ARGUMENT.toString());
            }

            String url = stockServiceUrl + "/stock/unreserve?";
            HttpEntity<String> request = new HttpEntity<>(new HttpHeaders());
            List<OrderEntity> orderEntities = orderRepository.findAllByOrderId(orderId);

            if (orderEntities.isEmpty()) {
                throw new BusinessLogicException("Order not found", ErrorCodes.ORDER_NOT_FOUND.toString());
            }

            try {
                orderEntities.forEach(entity -> {
                    String params = "itemId=" + entity.getItemId() + "&count=" + entity.getCount();
                    restTemplate.postForEntity(url + params, request, String.class);
                    orderRepository.deleteById(entity.getId());
                });
            } catch (HttpServerErrorException.InternalServerError e) {
                Map<String, String> map = mapper.readValue(e.getResponseBodyAsString(), Map.class);
                if (map.containsKey("errorCode")) {
                    throw new BusinessLogicException(map.get("message"), map.get("errorCode"));
                } else {
                    throw e;
                }
            }

            logger.info("Method .deleteOrder completed");

        } catch (Exception e) {
            logger.error("Failed to .deleteOrder error={}", e.toString(), e);
            throw e;
        }
    }

    private void validateOrder(Order order) {

        if (Objects.isNull(order)) {
            throw new BusinessLogicException("Order can't be null",
                    ErrorCodes.ILLEGAL_ARGUMENT.toString());
        }

        String orderId = order.getOrderId();
        String phoneNumber = order.getPhoneNumber();
        String address = order.getAddress();
        String orderDate = order.getOrderDate();
        String deliveryDate = order.getDeliveryDate();
        Map<String, Integer> items = order.getItems();

        if (Objects.isNull(orderId) || orderId.equals("") ||
                Objects.isNull(phoneNumber) || phoneNumber.equals("") ||
                Objects.isNull(address) || address.equals("") ||
                Objects.isNull(orderDate) || Objects.isNull(deliveryDate) ||
                Objects.isNull(items) || items.isEmpty() ||
                items.containsKey(null) || items.containsValue(null) ||
                items.containsKey("")) {
            throw new BusinessLogicException("Order fields can't be null or empty",
                    ErrorCodes.ILLEGAL_ARGUMENT.toString());
        }
    }

    private List<OrderEntity> convertOrderToOrderEntities(Order order) {

        List<OrderEntity> entities = new ArrayList<>();
        order.getItems().forEach((itemId, count) -> entities.add(
                OrderEntity.builder()
                        .orderId(order.getOrderId())
                        .phoneNumber(order.getPhoneNumber())
                        .address(order.getAddress())
                        .orderDate(order.getOrderDate())
                        .deliveryDate(order.getDeliveryDate())
                        .itemId(itemId)
                        .count(count)
                        .build()));
        return entities;
    }

    private Order convertOrderEntityToOrder(List<OrderEntity> orderEntities) {
        Order order = Order.builder()
                .orderId(orderEntities.get(0).getOrderId())
                .phoneNumber(orderEntities.get(0).getPhoneNumber())
                .address(orderEntities.get(0).getAddress())
                .deliveryDate(orderEntities.get(0).getDeliveryDate())
                .orderDate(orderEntities.get(0).getOrderDate())
                .build();
        for (OrderEntity orderEntity : orderEntities) {
            order.addItem(orderEntity.getItemId(), orderEntity.getCount());
        }
        return order;
    }
}
