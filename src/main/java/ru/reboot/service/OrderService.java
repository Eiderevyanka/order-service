package ru.reboot.service;

import ru.reboot.dto.Order;

import java.util.List;

public interface OrderService {

    /**
     * Получить все заказы
     */
    List<Order> getAllOrders();

    /**
     * Получить информацию о заказе по Id
     */
    Order getOrderById(String orderId);

    /**
     * Получить информацию о заказе по номеру телефона
     */
    List<Order> getOrdersByPhoneNumber(String phoneNumber);

    /**
     * Создать новый заказ
     */
    Order createOrder(Order order) throws Exception;

    /**
     * Удалить заказ
     */
    void deleteOrder(String orderId) throws Exception;
}
