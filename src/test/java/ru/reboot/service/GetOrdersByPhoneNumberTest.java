package ru.reboot.service;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dao.OrderRepository;
import ru.reboot.dao.entity.OrderEntity;
import ru.reboot.dto.Order;
import ru.reboot.error.BusinessLogicException;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class GetOrdersByPhoneNumberTest {

    OrderEntity orderEntityOne = OrderEntity.builder()
            .orderId("1").phoneNumber("111111111").address("Street1")
            .deliveryDate(LocalDateTime.now().minus(1, ChronoUnit.DAYS).toString())
            .orderDate(LocalDateTime.now().toString()).itemId("one").count(1).build();

    OrderEntity orderEntityTwo = OrderEntity.builder()
            .orderId("2").phoneNumber("222222222").address("Street2")
            .deliveryDate(LocalDateTime.now().minus(2, ChronoUnit.DAYS).toString())
            .orderDate(LocalDateTime.now().toString()).itemId("two").count(2).build();

    OrderEntity orderEntityThree = OrderEntity.builder()
            .orderId("1").phoneNumber("333333333").address("Street3")
            .deliveryDate(LocalDateTime.now().minus(1, ChronoUnit.DAYS).toString())
            .orderDate(LocalDateTime.now().toString()).itemId("three").count(3).build();

    OrderEntity orderEntityFour = OrderEntity.builder()
            .orderId("2").phoneNumber("444444444").address("Street2")
            .deliveryDate(LocalDateTime.now().minus(2, ChronoUnit.DAYS).toString())
            .orderDate(LocalDateTime.now().toString()).itemId("four").count(4).build();

    @Test
    public void getOrdersByPhoneNumber_Positive() {

        OrderRepository orderRepositoryMock = Mockito.mock(OrderRepository.class);
        OrderServiceImpl orderService = new OrderServiceImpl();

        orderService.setOrderRepository(orderRepositoryMock);

        Mockito.when(orderRepositoryMock.findDistinctOrderId()).thenReturn(Collections.singletonList(orderEntityOne.getOrderId()));

        Mockito.when(orderRepositoryMock.findAllByOrderId(Mockito.anyString())).thenReturn(Arrays.asList(orderEntityOne, orderEntityThree));

        List<Order> orderList = orderService.getOrdersByPhoneNumber(orderEntityOne.getPhoneNumber());

        Map<String, Integer> mapOne = new HashMap<>();
        mapOne.put(orderEntityOne.getItemId(), orderEntityOne.getCount());
        mapOne.put(orderEntityThree.getItemId(), orderEntityThree.getCount());

        Assert.assertEquals(orderEntityOne.getPhoneNumber(), orderList.get(0).getPhoneNumber());
        Assert.assertEquals(orderList.get(0).getItems(), mapOne);

        Mockito.when(orderRepositoryMock.findDistinctOrderId()).thenReturn(Collections.singletonList(orderEntityTwo.getOrderId()));

        Mockito.when(orderRepositoryMock.findAllByOrderId(Mockito.anyString())).thenReturn(Arrays.asList(orderEntityTwo, orderEntityFour));

        orderList = orderService.getOrdersByPhoneNumber(orderEntityTwo.getPhoneNumber());

        Map<String, Integer> mapTwo = new HashMap<>();
        mapTwo.put(orderEntityTwo.getItemId(), orderEntityTwo.getCount());
        mapTwo.put(orderEntityFour.getItemId(), orderEntityFour.getCount());

        Assert.assertEquals(orderEntityTwo.getPhoneNumber(), orderList.get(0).getPhoneNumber());
        Assert.assertEquals(orderList.get(0).getItems(), mapTwo);

        Mockito.verify(orderRepositoryMock, Mockito.times(2)).findAllByOrderId(Mockito.anyString());
    }


    @Test
    public void getOrdersByPhoneNumber_WhenIllegalArgument() {

        OrderRepository orderRepositoryMock = Mockito.mock(OrderRepository.class);
        OrderServiceImpl orderService = new OrderServiceImpl();

        orderService.setOrderRepository(orderRepositoryMock);

        Mockito.when(orderRepositoryMock.findAllByOrderId("")).thenThrow(BusinessLogicException.class);

        try {
            orderService.getOrdersByPhoneNumber(null);
            Assert.fail();

        } catch (BusinessLogicException ex) {

            Assert.assertEquals(ex.getCode(), "ILLEGAL_ARGUMENT");
        }
    }
}