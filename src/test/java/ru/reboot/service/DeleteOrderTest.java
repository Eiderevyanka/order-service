package ru.reboot.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import ru.reboot.dao.OrderRepository;
import ru.reboot.dao.entity.OrderEntity;
import ru.reboot.dto.Order;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCodes;
import ru.reboot.service.OrderServiceImpl;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class DeleteOrderTest {

    private OrderRepository orderRepository = mock(OrderRepository.class);
    private OrderServiceImpl orderService = new OrderServiceImpl();
    private RestTemplate restTemplate = mock(RestTemplate.class);

    {
        orderService.setOrderRepository(orderRepository);
        orderService.setRestTemplate(restTemplate);
    }

    @Test
    public void deleteOrderTestWrongData() throws Exception {


        try {
            orderService.deleteOrder(null);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), ErrorCodes.ILLEGAL_ARGUMENT.toString());
        }

        try {
            orderService.deleteOrder("");
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), ErrorCodes.ILLEGAL_ARGUMENT.toString());
        }
    }

    @Test
    public void deleteOrderTestPositive() throws Exception {

        when(restTemplate.postForEntity(anyString(), any(), any())).thenReturn(any());
        when(orderRepository.findAllByOrderId("itemId"))
                .thenReturn(Collections.singletonList(OrderEntity.builder().build()));
        orderService.deleteOrder("itemId");
        verify(orderRepository).findAllByOrderId("itemId");
        verify(orderRepository).deleteById(anyInt());
        verify(restTemplate).postForEntity(anyString(), any(), any());
    }
}