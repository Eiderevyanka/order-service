package ru.reboot.service;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.web.client.RestTemplate;
import ru.reboot.dao.OrderRepository;
import ru.reboot.dao.entity.OrderEntity;
import ru.reboot.dto.Order;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCodes;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.mockito.Mockito.*;

public class CreateOrderTest {

    private OrderRepository orderRepository = mock(OrderRepository.class);
    private OrderServiceImpl orderService = new OrderServiceImpl();
    private RestTemplate restTemplate = mock(RestTemplate.class);
    private Order order;

    {
        orderService.setOrderRepository(orderRepository);
        orderService.setRestTemplate(restTemplate);
        order = Order.builder()
                .orderId("1")
                .phoneNumber("88008008080")
                .address("1")
                .orderDate(LocalDateTime.now().toString())
                .deliveryDate(LocalDateTime.MAX.toString())
                .addItem("2", 2)
                .addItem("3", 3)
                .build();
    }

    @Test
    public void createOrderTestWrongData() throws Exception {

        try {
            orderService.createOrder(null);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), ErrorCodes.ILLEGAL_ARGUMENT.toString());
        }

        try {
            order.setOrderId("");
            orderService.createOrder(order);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), ErrorCodes.ILLEGAL_ARGUMENT.toString());
        }

        when(restTemplate.postForEntity(anyString(), any(), any()))
                .thenThrow(new BusinessLogicException("", ErrorCodes.RESERVATION_ERROR.toString()));

        try {
            order.setOrderId("sasd");
            orderService.createOrder(order);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), ErrorCodes.RESERVATION_ERROR.toString());
        }
    }

    @Test
    public void createOrderTestOrderAlreadyExists() throws Exception {

        when(orderRepository.findAllByOrderId(anyString()))
                .thenReturn(Collections.singletonList(OrderEntity.builder().build()));

        try {
            orderService.createOrder(order);
            verify(orderRepository).findAllByOrderId(anyString());
            verifyZeroInteractions(orderRepository.save(any()));
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(ErrorCodes.DUPLICATE_ORDER.toString(), e.getCode());
        }
    }

    @Test
    public void createOrderTestPositive() throws Exception {

        when(orderRepository.findAllByOrderId(anyString())).thenReturn(Collections.emptyList());
        when(restTemplate.postForEntity(anyString(), any(), any())).thenReturn(any());

        Order createdOrder = orderService.createOrder(order);

        verify(orderRepository).findAllByOrderId(anyString());
        verify(orderRepository, Mockito.times(2)).save(any());
        verify(restTemplate, Mockito.times(2)).postForEntity(anyString(), any(), any());
        Assert.assertEquals(createdOrder, order);
    }
}